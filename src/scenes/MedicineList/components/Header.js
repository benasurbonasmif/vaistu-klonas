import React from 'react'
import PropTypes from 'prop-types';
import { Text, View } from 'react-native'

const Header = (props) => {
  return (
    <View>
      <Text>{props.text}</Text>
    </View>
  );
}

export default Header;

Header.propTypes = {
  text: PropTypes.string.isRequired
}