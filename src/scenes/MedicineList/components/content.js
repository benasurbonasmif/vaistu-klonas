import React from 'react'
import PropTypes from 'prop-types';
import { Text, View } from 'react-native'

const Mapper = (obj) => {
  if(Array.isArray(obj.time)){
    return obj.time.map((dataPoint, i)=>{
      return(<Text key={i}>{dataPoint.time}</Text>)
    })
  }
  else{
    return <Text>No Time</Text>
  }
}

const Content = (props) => {
  return (
    <View>
      {Mapper(props.data)}
    </View>
  );
}

export default Content;

Content.propTypes = {
  text: PropTypes.string
}