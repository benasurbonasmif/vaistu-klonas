import React from 'react'
import { Text } from 'react-native'
import loading from '../../utils/loading'
import Header from './components/Header'
import Accordion from 'react-native-collapsible/Accordion'
import Content from './components/content'

export default class MedicineList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sections: [],
            activeSections: []
        }

        this.renderHeader = this.renderHeader.bind(this)
        this.renderContent = this.renderContent.bind(this)
        this.updateSections = this.updateSections.bind(this)
    }
    componentDidMount(){
        return this._sub = this.props.navigation.addListener( 'didFocus', () => {
            return loading('medicines')
            .then((response) => {
                this.setState({sections: response})
            })
            .catch((error) =>{
                console.error(error);
              });
            })
    }

    renderHeader = section => {
        return (
            <Header text={section.name} />
        );
    };

    renderContent = section => {
        return (
            <Content data={section} />
        );
    };

    updateSections = activeSections => {
        this.setState({ activeSections });
    };

    render() {
        return (
            <Accordion
                sections={this.state.sections}
                activeSections={this.state.activeSections}
                renderHeader={this.renderHeader}
                renderContent={this.renderContent}
                onChange={this.updateSections}
                expandMultiple={true}
            />
        )
    }
}