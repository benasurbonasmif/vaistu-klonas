import React from 'react'
import {View, Text} from 'react-native'
import InputField from  '../../../components/common/InputField'
import DateTimePicker from 'react-native-modal-datetime-picker'
import TimeField from './TimeField/TimeField'
import ButtonStyled from '../../../components/common/ButtonStyled'
import saveMedicine from '../../../utils/saving'


export default class MedicineReminderForm extends React.Component {
    constructor(props) {
        super(props);
        var d = new Date()
        var hour = d.getHours().toString()
        var minute = d.getMinutes().toString()
        if(minute.length < 2) {minute = '0'+minute}
        this.state = {
            name: '',
            time: hour + ':' + minute,
            isTimePickerVisible: false
        }
        this.nameChange = this.nameChange.bind(this)
        this.showTimePicker = this.showTimePicker.bind(this)
        this.hideTimePicker = this.hideTimePicker.bind(this)
        this.timePicked = this.timePicked.bind(this)
        this.saveMedicines = this.saveMedicines.bind(this)
    }

    saveMedicines(){
        const dataObj = {
            'name' : this.state.name,
            'time' : this.state.time
        }
        saveMedicine(dataObj)
    }

    nameChange(e) {
        this.setState({
            name: e
        })
    }

    showTimePicker() {
        this.setState({
            isTimePickerVisible: true
        })
    }

    timePicked(t) {
        var hour = t.getHours().toString()
        var minute = t.getMinutes().toString()
        if(minute.length < 2){minute = '0'+minute}
        this.setState({
            isTimePickerVisible: false,
            time: hour + ':' + minute,
            hours: hour,
            minutes: minute
        })
    }

    hideTimePicker() {
        this.setState({
            isTimePickerVisible: false
        })
    }

    render() {
        return (
            <View>
                <Text>{this.state.time}</Text>

                <InputField label='Name' onChange={this.nameChange} />

                <TimeField 
                    label='Time' 
                    time={this.state.time}
                    openPicker={this.showTimePicker}
                />

                <ButtonStyled title='Save' onPress={this.saveMedicines} />

                <DateTimePicker
                    isVisible={this.state.isTimePickerVisible}
                    onConfirm={this.timePicked}
                    onCancel={this.hideTimePicker}
                    mode='time'
                    confirmBtnText='Confirm'
                    cancelBtnText='Cancel'
                    format='HH:MM'
                />
            </View>
        );
    }
}