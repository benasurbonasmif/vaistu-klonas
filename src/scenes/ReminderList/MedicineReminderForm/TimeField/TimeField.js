import React from 'react'
import { TextInput, View, Text, Button } from 'react-native'
import Label from '../../../../components/common/Label'
import ButtonStyled from '../../../../components/common/ButtonStyled'
import PropTypes from 'prop-types'

const TimeField = (props) => {
  return (
    <View>
      <Label text={props.label} />
      <Text>{props.time}</Text>
      <ButtonStyled onPress={props.openPicker} title='Pick' />
    </View>
  );
}

export default TimeField;

TimeField.propTypes = {
  label: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
  openPicker: PropTypes.func.isRequired
}