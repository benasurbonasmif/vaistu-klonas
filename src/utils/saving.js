import getObject from './loading'
import { AsyncStorage } from 'react-native'

// Gets medicines object from loading component, tries to find same name object, if there is none
// then creates temporary object with needed format and then pushes it to existing list. If there
// is object with same name, then adds time as object to time list
// After all formatting, stringifies and pushes existing medicines object to Async Storage
export default saveMedicine = async (dataObj) => {
    let existingMedicines = await getObject('medicines').then((value) => {
        return value
    })
    let indexOfProvidedName = existingMedicines.findIndex(element => element.name == dataObj.name)
    if (indexOfProvidedName === -1) {
        let tempObj = {
            name: dataObj.name,
            time: [{ time: dataObj.time }]
        }
        existingMedicines.push(tempObj)
    }
    else {
        //Add only if same time doesn't exist, otherwise do nothing
        if (existingMedicines[indexOfProvidedName].time.findIndex(element => element.time == dataObj.time) === -1) {
            existingMedicines[indexOfProvidedName].time.push({ time: dataObj.time })
        }
    }

    return await AsyncStorage.setItem('medicines', JSON.stringify(existingMedicines)).catch(function (error) {
        console.log('There has been a problem with saving: ' + error.message);
        throw error;
    });
}