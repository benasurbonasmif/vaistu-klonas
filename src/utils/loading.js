import {AsyncStorage} from 'react-native'


export default getObject = async (name) => { 
    return await AsyncStorage.getItem(name).then( (obj) => {
        let object = JSON.parse(obj)
        if (!object){
            return []
        }
        else return object
    }).catch(function(error) {
        console.log('There has been a problem with loading: ' + error.message);
          throw error;
        })
       
}