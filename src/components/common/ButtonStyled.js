import React from 'react'
import {TouchableOpacity, Text} from 'react-native'
import PropTypes from 'prop-types'

const ButtonStyled = (props) => {
    return <TouchableOpacity onPress={props.onPress}><Text>{props.title}</Text></TouchableOpacity>;
}

export default ButtonStyled;

ButtonStyled.propTypes = {
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
}