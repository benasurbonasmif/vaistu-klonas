import React from 'react'
import {TextInput, View} from 'react-native'
import Label from './Label'
import PropTypes from 'prop-types'

const InputField = (props) => {
   return (
    <View>
        <Label text={props.label}/>    
        <TextInput onChangeText={props.onChange}></TextInput>
    </View>
   );
}

export default InputField;

InputField.propTypes = {
    label: PropTypes.string.isRequired,
    onCHange: PropTypes.func
}