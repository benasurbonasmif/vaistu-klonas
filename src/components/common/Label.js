import React from 'react'
import PropTypes from 'prop-types';
import {Text} from 'react-native'

const Label = (props) => {
    return (<Text>{props.text}</Text>);
}

export default Label;

Label.propTypes = {
    text: PropTypes.string.isRequired
}