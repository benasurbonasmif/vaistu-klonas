/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import './config/ReactotronConfig'
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs'
import ReminderList from './src/scenes/ReminderList/ReminderList'
import MorePage from './src/scenes/MorePage/MorePage'
import MedicineList from './src/scenes/MedicineList/MedicineList'
import MedicineReminderForm from './src/scenes/ReminderList/MedicineReminderForm/MedicineReminderForm'

export default createMaterialBottomTabNavigator({
  Form: MedicineReminderForm,
  Main: ReminderList,
  Medicines: MedicineList,
  More: MorePage
})